import { Point } from "../point/Point";
import { Ship } from "./Ship";

describe("Ship class", () => {
  it("Should create basic point", () => {
    const player = new Ship(
      new Point(1, 2).position,
      new Point(3, 4).position,
      600
    );

    expect(player.power).toEqual(600);
    expect(player.position.quadrant).toEqual({ x: 1, y: 2 });
    expect(player.position.sector).toEqual({ x: 3, y: 4 });
  });
});
