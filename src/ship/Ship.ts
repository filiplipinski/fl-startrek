import { PointType } from "../point/Point.types";

export class Ship {
  private _power: number;
  private _quadrant: PointType;
  private _sector: PointType;

  constructor(quadrant: PointType, sector: PointType, power: number) {
    this._quadrant = quadrant;
    this._sector = sector;
    this._power = power;
  }

  setPosition(quadrant: PointType, sector: PointType) {
    this._quadrant = quadrant;
    this._sector = sector;
  }
  reducePower(amount: number) {
    this._power -= amount;
  }

  get position() {
    return {
      quadrant: this._quadrant,
      sector: this._sector,
    };
  }

  get power() {
    return this._power;
  }
}
