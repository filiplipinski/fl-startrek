import { StarTrek } from "./starTrek/StarTrek";
import { readCommand, readVector } from "./inputReader/inputReader";
import { CommandsEnum } from "./inputReader/inputReader.enum";
import { renderQuadrant } from "./mapRender/MapRender";

enum ModesEnum {
  Manoeuvre = "manoeuvre",
  Battle = "battle",
  Commands = "commands",
}

let mode = ModesEnum.Commands as ModesEnum;
const starTrek = new StarTrek();

starTrek.init();
console.log("GAME STARTED! \n \n");

while (starTrek.state === "on") {
  switch (mode) {
    case ModesEnum.Manoeuvre:
      const vector = readVector();

      starTrek.movePlayer(vector[0], vector[1]);

      const shortScannedMap = renderQuadrant(starTrek.player);
      console.log(shortScannedMap);
      mode = ModesEnum.Commands;
      break;
    case ModesEnum.Battle:
      console.log("//Battle not implemented yet, returning to COMMANDS mode");
      mode = ModesEnum.Commands;
      break;
    case ModesEnum.Commands:
      const command = readCommand();

      switch (command) {
        case CommandsEnum.Manoeuvre:
          console.log("ENTERING INTO THE MANOEUVRE MODE");
          mode = ModesEnum.Manoeuvre;
          break;
        case CommandsEnum.ShortScan:
          console.log("Short scan results: \n");
          const shortScannedMap = renderQuadrant(starTrek.player);
          console.log(shortScannedMap);
          mode = ModesEnum.Commands;
          break;
        case CommandsEnum.LongScan:
          console.log(
            "//Long Scan not implemented yet, returning to COMMANDS mode"
          );
          mode = ModesEnum.Commands;
          break;
        case CommandsEnum.Battle:
          console.log("ENTERING INTO THE BATTLE MODE");
          mode = ModesEnum.Battle;
          break;
        case CommandsEnum.GalaxyMap:
          console.log("Galaxy map results: \n");
          const galaxyMap = renderQuadrant(starTrek.player);
          console.log(galaxyMap);
          break;
      }
      break;
  }
}
