import { PointType } from "../point/Point.types";

export const convertPositionToString = (
  sector: PointType,
  quadrantSize: number
) => {
  let rowLength = 3 * quadrantSize + 4;
  return rowLength * quadrantSize - sector.y * rowLength + 2 + sector.x * 3;
};
