import { quadrantConfig } from "../config/starTrekConfig";
import { PointType } from "../point/Point.types";
import { Ship } from "../ship/Ship";
import { convertPositionToString } from "./mapRender.utils";

export const renderEmptyQuadrant = () => {
  const { size } = quadrantConfig;

  let map = "  ";
  for (let i = 0; i < size; i++) {
    map += `_${i}_`;
  }
  map += "\n";

  for (let x = 0; x < size; x++) {
    map += size - 1 - x + "|";
    for (let y = 0; y < size; y++) {
      map += " ∙ ";
    }
    map += "|" + "\n";
  }
  return map;
};

const applySymbolToMap: (
  map: string,
  playerPosition: PointType,
  symbol: string
) => string = (map, playerPosition, symbol) => {
  const playerPositionInMapString = convertPositionToString(
    playerPosition,
    quadrantConfig.size
  );

  return (
    map.substr(0, playerPositionInMapString) +
    symbol +
    map.substr(playerPositionInMapString + 3)
  );
};

export const renderQuadrant = (player: Ship, state?: unknown) => {
  let render = renderEmptyQuadrant();

  // adding player to map
  render = applySymbolToMap(
    render,
    player.position.sector,
    quadrantConfig.symbols.player
  );

  return render;
};
