import { Point } from "../point/Point";
import { Ship } from "../ship/Ship";
import { renderEmptyQuadrant, renderQuadrant } from "./mapRender";

describe("Test renderEmptyQuadrant", () => {
  it("Should display quadrant of given schema", () => {
    const expected = `  _0__1__2__3__4__5__6__7_
7| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
6| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
5| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
4| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
3| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
2| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
1| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
0| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
`;

    let result = renderEmptyQuadrant();
    expect(result).toMatch(expected);
  });
});

describe("Test renderQuadrant", () => {
  const player = new Ship(
    new Point(2, 2).position,
    new Point(2, 2).position,
    600
  );

  it("Should display player", () => {
    const expected = `  _0__1__2__3__4__5__6__7_
7| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
6| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
5| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
4| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
3| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
2| ∙  ∙  <*>∙  ∙  ∙  ∙  ∙ |
1| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
0| ∙  ∙  ∙  ∙  ∙  ∙  ∙  ∙ |
`;

    let result = renderQuadrant(player, []);
    expect(result).toMatch(expected);
  });
});
