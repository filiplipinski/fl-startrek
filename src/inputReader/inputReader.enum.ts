export enum CommandsEnum {
  Manoeuvre = 0,
  ShortScan = 1,
  LongScan = 2,
  Battle = 3,
  GalaxyMap = 4,
}
