import { question } from "readline-sync";
import { CommandsEnum } from "./inputReader.enum";

export const readCommand: () => CommandsEnum = () => {
  console.log(
    "Available commands: 0 = Manoeuvre, 1 = Short scan, 2 = Long scan, 3 = Battle, 4 = Galaxy map \n"
  );

  return parseInt(
    question("Enter command: ", {
      limit: [
        CommandsEnum.Manoeuvre,
        CommandsEnum.ShortScan,
        CommandsEnum.LongScan,
        CommandsEnum.Battle,
        CommandsEnum.GalaxyMap,
      ],
      limitMessage:
        "Incorrect command, please use one of these commands: $<limit>",
    })
  );
};

export const readVector = () => {
  return question("VECTOR ? ", {
    limit: [/^-?\d+,-?\d+$/],
    limitMessage:
      "INCORRECT VECTOR FORMAT, PLEASE USE THE FOLLOWING FORMAT: X,Y",
  })
    .split(",")
    .map((x: string) => parseInt(x));
};

export const readAmountOfEnergy = () => {
  return parseInt(
    question("How much energy use to attack? ", {
      limit: [/\d+/],
      limitMessage: "INCORRECT AMOUNT OF ENERGY",
    })
  );
};

export const readRestartDecision = () => {
  return question("Do you want to start again? (y/n): ", {
    limit: [/y|n/],
    limitMessage: "INCORRECT ANSWER Y/N",
  });
};
