import { PointType } from "./Point.types";

// x - horizontal
// y - vertical
export class Point {
  private readonly _x: number;
  private readonly _y: number;

  constructor(x: number, y: number) {
    this._x = x;
    this._y = y;
  }

  get position(): PointType {
    return {
      x: this._x,
      y: this._y,
    };
  }
}
