import { Point } from "./Point";

describe("Point class", () => {
  it("Should create basic point", () => {
    const newPoint = new Point(1, 2).position;

    expect(newPoint).toEqual({ x: 1, y: 2 });
  });
});
