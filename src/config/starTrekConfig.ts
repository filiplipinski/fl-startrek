import { Point } from "../point/Point";

export const starTrekConfig = {
  GALAXY: {
    QUADRANTS: 64,
    SECTORS_IN_QUADRANT: 64,
    KLINGON_SHIPS: 7,
    STARBASES: 2,
    STARS: 20,
  },
  MAX_POWER: 600,
  INITIAL_STARDATES: 30,
  initialQuadrant: new Point(0, 3).position,
  initialSector: new Point(3, 3).position,
};

export const quadrantConfig = {
  size: 8,
  symbols: {
    player: "<*>",
    ship: "+++",
    base: ">!<",
    star: " * ",
  },
};
