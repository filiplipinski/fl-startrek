export const getDistance = (x1: number, y1: number, x2: number, y2: number) => {
  return Math.abs(x1 - x2) + Math.abs(y1 - y2);
};

export const isInRange = (value: number, min: number, max: number) => {
  if (min > max) throw new Error("Min value have to be smaller than Max value");
  return value >= min && value <= max;
};
