import { PointType } from "../point/Point.types";
import { starTrekConfig } from "../config/starTrekConfig";
import { Point } from "../point/Point";

export const getSectorNumberFromPosition = ({
  quadrant,
  sector,
}: {
  quadrant: PointType;
  sector: PointType;
}) => {
  const sectorsInQuadrant = starTrekConfig.GALAXY.SECTORS_IN_QUADRANT;
  const numberOfQuadrants = starTrekConfig.GALAXY.QUADRANTS;

  const sectorNumber =
    quadrant.x * Math.sqrt(numberOfQuadrants) +
    quadrant.y * Math.sqrt(numberOfQuadrants) * sectorsInQuadrant +
    sector.x +
    sector.y * sectorsInQuadrant;

  return sectorNumber;
};

export const getGlobalPositionFromSectorNumber = (sectorNumber: number) => {
  const sectorsInQuadrant = starTrekConfig.GALAXY.SECTORS_IN_QUADRANT;
  const numberOfQuadrants = starTrekConfig.GALAXY.QUADRANTS;

  return new Point(
    sectorNumber % sectorsInQuadrant,
    Math.floor(sectorNumber / numberOfQuadrants)
  );
};

export const getQuadrantFromSectorNumber = (sectorNumber: number) => {
  const sectorsInQuadrant = starTrekConfig.GALAXY.SECTORS_IN_QUADRANT;
  const numberOfQuadrants = starTrekConfig.GALAXY.QUADRANTS;

  const y = Math.floor(
    sectorNumber / (Math.sqrt(numberOfQuadrants) * sectorsInQuadrant)
  );
  const x =
    Math.floor(sectorNumber / Math.sqrt(numberOfQuadrants)) %
    Math.sqrt(sectorsInQuadrant);
  return new Point(x, y);
};

export const getSectorFromSectorNumber = (sectorNumber: number) => {
  const sectorsInQuadrant = starTrekConfig.GALAXY.SECTORS_IN_QUADRANT;
  const y =
    Math.floor(sectorNumber / sectorsInQuadrant) % Math.sqrt(sectorsInQuadrant);
  const x = sectorNumber % Math.sqrt(sectorsInQuadrant);
  return new Point(x, y);
};
