import { Ship } from "../ship/Ship";
import { starTrekConfig } from "../config/starTrekConfig";
import { Point } from "../point/Point";
import {
  getGlobalPositionFromSectorNumber,
  getQuadrantFromSectorNumber,
  getSectorFromSectorNumber,
  getSectorNumberFromPosition,
} from "./StarTrek.utils";
import { getDistance, isInRange } from "../utils/utils";

export class StarTrek {
  private _state: "on" | "off";
  private _gameOver: boolean = false;
  private readonly _player: Ship;

  constructor() {
    this._state = "off";
    this._player = new Ship(
      starTrekConfig.initialQuadrant,
      starTrekConfig.initialSector,
      starTrekConfig.MAX_POWER
    );
  }

  init() {
    this._state = "on";
  }

  movePlayer(xDistance: number, yDistance: number) {
    const previousSectorNumber = getSectorNumberFromPosition(
      this._player.position
    );
    const previousGlobalPos = getGlobalPositionFromSectorNumber(
      previousSectorNumber
    );

    const newSectorNumber = previousSectorNumber + xDistance + yDistance * 64;

    const newQuadrant = getQuadrantFromSectorNumber(newSectorNumber);
    const newSector = getSectorFromSectorNumber(newSectorNumber);

    const currentGlobalPos = getGlobalPositionFromSectorNumber(newSectorNumber);

    const distanceTraveled = getDistance(
      previousGlobalPos.position.x,
      previousGlobalPos.position.y,
      currentGlobalPos.position.x,
      currentGlobalPos.position.y
    );
    if (
      !(
        isInRange(previousGlobalPos.position.x + xDistance, 0, 63) &&
        isInRange(previousGlobalPos.position.y + yDistance, 0, 63)
      )
    ) {
      console.log("You cannot leave galaxy!");
      return;
    }

    if (!(this.player.power - distanceTraveled > 0)) {
      console.log("You do not have enough power");
      this._gameOver = true;
      return;
    }

    this._player.setPosition(newQuadrant.position, newSector.position);
    this._player.reducePower(distanceTraveled);
  }

  get state() {
    return this._state;
  }
  get player() {
    return this._player;
  }
  get gameOver() {
    return this._gameOver;
  }
}
