import { starTrekConfig } from "../config/starTrekConfig";
import { Point } from "../point/Point";
import { Ship } from "../ship/Ship";
import { StarTrek } from "./StarTrek";

const starTrek = new StarTrek();

describe("StarTrek class", () => {
  beforeAll(() => starTrek.init());

  it("Should generate player with default config", () => {
    expect(starTrek.player).toBeInstanceOf(Ship);
    expect(starTrek.player.power).toBe(starTrekConfig.MAX_POWER);
    expect(starTrek.player.position.quadrant).toEqual(
      starTrekConfig.initialQuadrant
    );
    expect(starTrek.player.position.sector).toEqual(
      starTrekConfig.initialSector
    );
  });
});

describe("move ship correctly ", () => {
  let starTrek: StarTrek;

  beforeEach(() => {
    starTrek = new StarTrek();
    starTrek.init();
    starTrek.player.setPosition(
      new Point(1, 1).position,
      new Point(1, 1).position
    );

    //   starTrek.player.power = 600;
  });

  it("Should move player", () => {
    starTrek.movePlayer(6, 6);
    const expected = {
      quadrant: new Point(1, 1).position,
      sector: new Point(7, 7).position,
    };
    expect(starTrek.player.position).toEqual(expected);
  });

  it("Should move player", () => {
    starTrek.movePlayer(7, 7);
    const expected = {
      quadrant: new Point(2, 2).position,
      sector: new Point(0, 0).position,
    };
    expect(starTrek.player.position).toEqual(expected);
  });

  it("Should move player", () => {
    starTrek.movePlayer(6, 7);
    const expected = {
      quadrant: new Point(1, 2).position,
      sector: new Point(7, 0).position,
    };
    expect(starTrek.player.position).toEqual(expected);
  });

  it("Should move player", () => {
    starTrek.movePlayer(-2, -2);
    const expected = {
      quadrant: new Point(0, 0).position,
      sector: new Point(7, 7).position,
    };
    expect(starTrek.player.position).toEqual(expected);
  });

  it("Should move player", () => {
    starTrek.movePlayer(0, 0);
    const expected = {
      quadrant: new Point(1, 1).position,
      sector: new Point(1, 1).position,
    };

    expect(starTrek.player.position).toEqual(expected);
  });
});

describe("not move ship correctly cause of boundaries", () => {
  let starTrek: StarTrek;

  const initialQuadrant = new Point(1, 1).position;
  const initialSector = new Point(1, 1).position;

  const expected = {
    quadrant: initialQuadrant,
    sector: initialSector,
  };

  beforeEach(() => {
    starTrek = new StarTrek();
    starTrek.init();
    starTrek.player.setPosition(initialQuadrant, initialSector);
  });

  it("Should leave position unchanged", () => {
    starTrek.movePlayer(-33, 0);
    expect(starTrek.player.position).toEqual(expected);
  });

  it("Should leave position unchanged", () => {
    starTrek.movePlayer(7, 66);
    expect(starTrek.player.position).toEqual(expected);
  });

  it("Should leave position unchanged", () => {
    starTrek.movePlayer(6, -11);
    expect(starTrek.player.position).toEqual(expected);
  });

  it("Should leave position unchanged", () => {
    starTrek.movePlayer(-277, -2);

    expect(starTrek.player.position).toEqual(expected);
  });

  it("Should leave position unchanged", () => {
    starTrek.movePlayer(-21, 0);
    expect(starTrek.player.position).toEqual(expected);
  });
});

describe("not move ship cause of no power", () => {
  let starTrek: StarTrek;

  const initialQuadrant = new Point(1, 1).position;
  const initialSector = new Point(1, 1).position;

  const expected = {
    quadrant: initialQuadrant,
    sector: initialSector,
  };

  beforeEach(() => {
    starTrek = new StarTrek();
    starTrek.init();
    starTrek.player.setPosition(initialQuadrant, initialSector);
  });

  it("Should leave position unchanged", () => {
    starTrek.movePlayer(30, 30);
    starTrek.movePlayer(-30, -30);
    starTrek.movePlayer(40, 40);
    starTrek.movePlayer(-40, -40);
    starTrek.movePlayer(40, 40);
    starTrek.movePlayer(-40, -40);
    starTrek.movePlayer(40, 40);
    starTrek.movePlayer(-40, -39);

    expect(starTrek.player.position).toEqual({
      quadrant: new Point(1, 1).position,
      sector: new Point(1, 2).position,
    });

    // one last point of power
    expect(starTrek.player.power).toEqual(1);

    starTrek.movePlayer(0, -1);
    // shouldn't move cause of no power
    expect(starTrek.player.position).toEqual({
      quadrant: new Point(1, 1).position,
      sector: new Point(1, 2).position,
    });
    // game over, cause ship has no power
    expect(starTrek.gameOver).toEqual(true);
  });
});
